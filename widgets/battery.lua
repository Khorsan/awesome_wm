local wibox = require("wibox")

batterywidget = wibox.widget.textbox()
batterywidget:set_align("right")
batterywidget:set_text("| Loading... |")


function batteryInfo(adapter)
	
	spacer = " "
	local fcurrent = io.open("/sys/class/power_supply/"..adapter.."/energy_now")    
	local fcap = io.open("/sys/class/power_supply/"..adapter.."/energy_full")
	local fstatus = io.open("/sys/class/power_supply/"..adapter.."/status")
	local current = fcurrent:read()
	local cap = fcap:read()
	local status = fstatus:read()
	local battery = math.floor(current * 100 / cap)
	
	if status:match("Charging") then
		dir = "↑"
		battery = "A/C ("..battery..")"
	elseif status:match("Discharging") then
		dir = "↓"
	
		if tonumber(battery) > 25 and tonumber(battery) < 75 then
			battery = battery
		elseif tonumber(battery) < 25 then
		
		if tonumber(battery) < 10 then
		naughty.notify({ title      = "Battery Warning"
		, text       = "Battery low!"..spacer..battery.."%"..spacer.."left!"
		, timeout    = 5
		, position   = "top_right"
		, fg         = beautiful.fg_focus
		, bg         = beautiful.bg_focus
		})
		end
		battery = battery
		else
			battery = battery
		end
	else
		dir = "="
		battery = "A/C"
	end
	
	battery = battery .."%"
	batterywidget:set_text(" | Battery: " .. battery .. spacer ..  dir .. " |")
	fcurrent:close()
	fcap:close()
	fstatus:close()
end
  
 battery_timer = timer({timeout = 1})
 battery_timer:connect_signal("timeout", function()
     batteryInfo("BAT1")
 end)
 battery_timer:start()
